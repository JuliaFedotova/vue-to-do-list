import { createRouter, createWebHistory } from 'vue-router';
import ToDoListView from '@/views/ToDoListView.vue';
import DeletedListView from '@/views/DeletedListView.vue';
import CheckedListView from '@/views/CheckedListView.vue';

const routes = [
  { path: '/', name: 'home', component: ToDoListView },
  { path: '/deleted', name: 'deleted', component: DeletedListView },
  { path: '/checked', name: 'checked', component: CheckedListView },
];

export const router = createRouter({
  history: createWebHistory(),
  routes,
});
