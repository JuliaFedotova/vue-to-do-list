import { ref } from 'vue';

const deletedNotes = ref<string[]>([]);
const notes = ref<string[]>([]);
const checkedNotes = ref<string[]>([]);

export function useNotes() {
  function addDeletedNote(note: string) {
    deletedNotes.value.push(note);
  }

  function addNote(note: string) {
    notes.value.push(note);
  }

  function checkNote(note: string) {
    checkedNotes.value.push(note);
  }

  return {
    addDeletedNote,
    addNote,
    checkNote,
    checkedNotes,
    deletedNotes,
    notes,
  };
}
